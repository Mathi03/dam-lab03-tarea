import React, {Component} from 'react';
import {
  View,
  Text,
  FlatList,
  TouchableOpacity,
  StyleSheet,
  Image,
  Alert,
} from 'react-native';

class myList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      lista: [
        {
          id: 1,
          name: 'Otis',
          avatar_url:
            'https://vignette.wikia.nocookie.net/sex-education-netflix/images/4/43/Otis_Milburn_Season_1_Portrait.jpg/revision/latest?cb=20190902201801',
          subtitle:
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
        },
        {
          id: 2,
          name: 'Trompijas',
          avatar_url:
            'https://instagram.faqp1-1.fna.fbcdn.net/v/t51.2885-15/e35/46840411_190924125196117_5225523161443274296_n.jpg?_nc_ht=instagram.faqp1-1.fna.fbcdn.net&_nc_cat=101&_nc_ohc=CfG-Unn72OcAX_W2LOg&oh=c72ec5fcc5f2c1aa16eb6c6c7e505a44&oe=5EC827A2',
          subtitle:
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
        },
        {
          id: 3,
          name: 'Adam',
          avatar_url:
            'https://vignette.wikia.nocookie.net/sex-education-netflix/images/3/3c/Adam_Groff_Season_1_Portrait.jpg/revision/latest?cb=20190902213051',
          subtitle:
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
        },
        {
          id: 4,
          name: 'Maeves',
          avatar_url:
            'https://media.metrolatam.com/2019/01/24/screenshot20190124at21506pm-074bc500050ae1a0634dece0ba32e08c-0x1200.jpg',
          subtitle:
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
        },
        {
          id: 3,
          name: 'Jackson',
          avatar_url:
            'https://vignette.wikia.nocookie.net/sex-education-netflix/images/5/59/Jackson_Marchetti_Season_1_Portrait.jpg/revision/latest?cb=20190902213612',
          subtitle:
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
        },
      ],
    };
  }
  keyExtractor = (item, index) => index.toString();
  onItem() {
    Alert.alert(
      'Seleccion',
      'Has seleccion un ITEM',
      /*[
            {
               text: "Aceptar",
               onPress: () => Alert.alert("Aceptar")
            },
            {
               text: "Cancelar",
               onPress: () => Alert.alert("Cancelar")
            }
         ]*/
    );
  }
  renderItem = ({item}) => (
    <TouchableOpacity
      style={{borderBottomWidth: 1}}
      onPress={this.onItem.bind(this)}>
      <View style={styles.itemContainer}>
        <View>
          <Image
            source={{uri: item.avatar_url}}
            style={styles.itemImage}
            resizeMode="cover"
          />
        </View>
        <View style={styles.description}>
          <Text style={styles.itemName}>{item.name}</Text>
          <Text style={styles.itemSubtitle}>{item.subtitle}</Text>
        </View>
      </View>
    </TouchableOpacity>
  );
  render() {
    return (
      <View style={styles.container}>
        <Text style={{fontSize: 20, alignSelf: 'center', fontWeight: 'bold'}}>
          LISTA DE PERSONAS
        </Text>
        <FlatList
          data={this.state.lista}
          renderItem={this.renderItem}
          keyExtractor={this.keyExtractor}
          //ItemSeparatorComponent={}
        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    marginTop: 15,
  },
  itemContainer: {
    flex: 1,
    flexDirection: 'row',
    marginLeft: 20,
    //backgroundColor: '#E9E9E9',
    justifyContent: 'flex-start',
    margin: 15,
  },
  itemName: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  itemSubtitle: {
    fontSize: 12,
    color: 'gray',
  },
  description: {
    marginLeft: 20,
    marginRight: 80,
  },
  itemImage: {
    width: 80,
    height: 80,
    borderRadius: 10,
  },
});
export default myList;
