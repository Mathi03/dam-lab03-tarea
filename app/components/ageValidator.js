import React, {Component} from 'react';
import {View, Text, StyleSheet, TextInput} from 'react-native';

class ageValidator extends Component {
  constructor(props) {
    super(props);
    this.state = {
      textAge: '',
    };
  }
  changeInput = texto => {
    console.log(texto);
    if (/^\d+$/.test(texto) || texto === '') {
      this.setState({
        textAge: texto,
      });
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.title}> Comprobacion de edad </Text>
        <TextInput
          style={styles.inputText}
          onChangeText={this.changeInput.bind(this)}
          keyboardType={'numeric'}
          value={this.state.textAge}
          placeholder={'Ingrese su edad'}
        />

        <View>
          {this.state.textAge == '' ? (
            <Text />
          ) : this.state.textAge >= 18 ? (
            <Text style={styles.response}>Usted es mayor de edad</Text>
          ) : (
            <Text style={styles.response}>Usted es menor de edad</Text>
          )}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'stretch',
    padding: 20,
  },
  title: {
    marginTop: 15,
    alignSelf: 'center',
    fontSize: 20,
    fontWeight: 'bold',
  },
  inputText: {
    marginTop: 15,
    borderRadius: 10,
    borderWidth: 1,
    height: 35,
  },
  response: {
    marginTop: 15,
    alignSelf: 'center',
    fontSize: 20,
    fontStyle: 'italic',
  },
});
export default ageValidator;

module.exports = ageValidator;
