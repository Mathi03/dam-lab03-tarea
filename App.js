import React, {Component} from 'react';
import {View, Text} from 'react-native';

import Lista from './app/components/myList';

const Validator = require('./app/components/ageValidator');

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View style={{flex: 1}}>
        <View>
          <Validator />
        </View>
        <View>
          <Lista />
        </View>
      </View>
    );
  }
}

export default App;
